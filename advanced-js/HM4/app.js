const root = document.getElementById("root");

const fetchUri = (uri) => {
    return fetch(uri, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

const getFilms = async () => {
    const res = await fetchUri('https://ajax.test-danit.com/api/swapi/films')
    return res.json();
}

const showFilms = (films) => {
    const filmsList = document.createElement('ul');
    filmsList.innerHTML = mapFilms(films);
    root.append(filmsList);
}

const showCharacters = async (films) => {
    const filmItems = document.querySelectorAll('li[data-filmId]');
    for (const item of filmItems) {
        const filmId = +item.dataset.filmid;
        const neededFilm = films.find(item => item.id === filmId);
        const characters = await getCharacters(neededFilm);
        const charactersStr = mapCharacters(characters);
        const charactersList = document.createElement("ol");
        charactersList.innerHTML = charactersStr;
        item.append(charactersList)
    }
}

const mapFilms = films => {
    return films.reduce((finalStr, film) => finalStr + `
    <li data-filmId=${film.id}>
        <p>Film name - ${film.name}</p>
        <p>Episode id - ${film.episodeId}</p>
        <p>Crawl - ${film.openingCrawl}</p>
        <p>Characters: </p>
    </li>
    `, "");
}

const mapCharacters = characters => {
    return characters.reduce((finalStr, char) => finalStr + `
     <li>${char.name}</li>
    `, "")
}

const getCharacters = async (film) => {
    const characters = [];
    for (const item of film.characters) {
        const res = await fetchUri(item);
        const char = await res.json();
        characters.push(char);
    }
    return characters;
}

const showFilmsWithCharacters = async () => {
    const films = await getFilms();
    showFilms(films);
    showCharacters(films);
}


showFilmsWithCharacters();



