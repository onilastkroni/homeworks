const parent = document.getElementById('root')
const ul = document.createElement('ul')

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

books.forEach(item => {
    try {
        if (!('price' in item)) throw new Error(`don't have property price in object`)
        if (!('name' in item)) throw new Error(`don't have property name in object`)
        if (!('author' in item)) throw new Error(`don't have property author in object`)
        ul.innerHTML += `<li>${item.name + item.author + item.price}</li>`
    } catch (e) {
        console.log(e.message);
    }
})

parent.append(ul)

