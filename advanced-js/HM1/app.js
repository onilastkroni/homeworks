class Employee {
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
        this._salary = salary;
    }

    get salary() {
        return this._salary * 3;
    }
}

const program1 = new Programmer('john', 15, 34, 2);
const program2 = new Programmer('nik', 21, 1700, 3)

console.log(program1.salary);
console.log(program2);
