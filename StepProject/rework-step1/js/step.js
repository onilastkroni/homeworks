const li1 = document.querySelectorAll('.types')
const ul2 = document.querySelectorAll('.img-text')
const ul1 = document.querySelector('.type')

ul1.addEventListener('click', (event) => {
    li1.forEach(elem => {
        if (elem.dataset.name === event.target.dataset.name) {
            elem.classList.add('active')

        } else if (elem.classList.contains('active')) {
            elem.classList.remove('active')
        }

    })


    ul2.forEach(elem => {
        if (elem.dataset.name === event.target.dataset.name) {
            elem.classList.add('content-active')
        } else if (elem.classList.contains('content-active')) {
            elem.classList.replace('content-active' , 'content-hidden')
        }
    })
})



$(".portfolio-theme").click((event) => {
    $(".portfolio-theme").removeClass("portfolio-active");
    $(event.target).addClass("portfolio-active");
    currentClass = $(event.target).attr('data-filter');
    if (currentClass != "all") {
        $(`.${currentClass}`).fadeIn("fast");
        $(".port-img").not(`.${currentClass}`).css("display", "none");
    } else {
        $(".port-img").css("display", "block");
    }
});


$('.rev-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    autoplay: false,
    autoplaySpeed: 3000,
    cssEase: 'linear'
});
$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.rev-slider',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0',
    cssEase: 'linear'
});

$(".portfolio-load").click(() => {
    $(".portfolio-block>.load-wrapp").css("display", "block");
    setTimeout(() => {
        $(".portfolio-block>.load-wrapp").css("display", "none");
        if (!loadBtnStatus) {
            for (let j = 4; j <= 6; j++) {
                $(".portfolio").append(`<div class='port-img ${graph}'><img src='images/portfolio/${graph}/${graph}${j}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${web}'><img src='images/portfolio/${web}/${web}${j}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${landing}'><img src='images/portfolio/${landing}/${landing}${j}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${wordpress}'><img src='images/portfolio/${wordpress}/${wordpress}${j}.jpg' alt='web'></div>`);
                loadBtnStatus = true;
            }
        } else {
            for (let k = 7; k <= 9; k++) {
                $(".portfolio").append(`<div class='port-img ${graph}'><img src='images/portfolio/${graph}/${graph}${k}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${web}'><img src='images/portfolio/${web}/${web}${k}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${landing}'><img src='images/portfolio/${landing}/${landing}${k}.jpg' alt='web'></div>`);
                $(".portfolio").append(`<div class='port-img ${wordpress}'><img src='images/portfolio/${wordpress}/${wordpress}${k}.jpg' alt='web'></div>`);
                $(".portfolio-load").hide();
            }
        }
        if (currentClass != "all" && currentClass != false) {
            $(`.${currentClass}`).css("display", "block");
            $(".port-img").not(`.${currentClass}`).css("display", "none");
        }
        getImgHover();
    }, 2000);
});
getImgHover();




