const express = require("express");

const app = express();

const PORT = process.env.PORT || 5000;

app.use('/', express.static("./"))


async function start() {
    try {
        //starting server
        app.listen(PORT, () => {
            console.log(`Server has been started on port ${PORT} ...`);
        });
    } catch (e) {
        //logging error
        console.log(e)
    }
}

start();