import React, {Component} from 'react';
import Button from "../Button/Button";
import Star from "../Star/Star";
import './Card.scss'
import Modal from "../Modal/Modal";
import PropTypes from 'prop-types';

class Card extends Component {
    state = {
        isClose: true,
        inStorage: false
    }

    openModal = () => {
        this.setState({
            isClose: !this.state.isClose
        })
    }
    setActive = (type) => {
        this.setState({
            isClose: type
        })
    }
    addToStorage = (type, item, name) => {
        if (type === 'cart') {
            localStorage.setItem(`cartHash${item}`, name)
            alert('Toy was added to cart!')
        } else {
            localStorage.setItem(`favourHash${item}`, name)
            alert('Toy was added to favour!')
            this.setState({
                inStorage: true
            })
        }
        this.setActive(true)
    }


    render() {
        const {name, price, url, color, article} = this.props
        const modal = (
            <Modal
                header={'Adding'}
                closeButton={true}
                text={"Add this toy to cart?"}
                action={<div>
                    <button className='modal__button' onClick={() => this.addToStorage('cart', article, name)}>Yes
                    </button>
                    <button className='modal__button' onClick={() => this.setActive(true)}>No</button>
                </div>}
                hidden={this.state.isClose}
                setActive={this.setActive}
            />)


        return (
            <div className={'card'} style={{background: color}}>
                <img width={250} height={250} src={url} alt=""/>
                <h4 className={"card-title"}>"{name}"</h4>
                <p className={"card-price"}>Price: {price}</p>
                <div className={'card-options'}>
                    <Star
                        handleClick={() => this.addToStorage('favour', article, name)} inStorage={this.state.inStorage}
                    />
                    <Button content={'ADD TO CART'} color={'green'} handleClick={this.openModal}/>
                </div>
                {!this.state.isClose && modal}
            </div>
        );

    }

    name
}

Card.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    color: PropTypes.string,

}



export default Card;