import './App.css';
import React, {Component} from "react";
import CardList from "./components/CardList/CardList";
import axios from "axios";

class App extends Component {
    state = {
        cardList: []
    }

    componentDidMount() {
        axios('/cardsList.json')
            .then(r => this.setState({cardList: r.data}))
    }


    render() {
        return(
        <div className="App">
            <CardList cards = {this.state.cardList}/>
        </div>
        )
    }
}

export default App;
