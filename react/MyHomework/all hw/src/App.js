import './App.css';
import React from "react";
import AppRoutes from "./routes/AppRoutes";
import MainMenu from "./components/MainMenu/MainMenu";

const App = () => {
  return (
    <div className="App">
      <MainMenu/>
      <AppRoutes/>
    </div>
  )
}

export default App;
