import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import CardList from "../CardList/CardList";
import './Favorites.scss'

const Favorites = () => {
  const favorList = useSelector(state => state.favorList.data)
  const cardList = useSelector(state => state.cardList.data)
  const dispatch = useDispatch()

  useEffect(() => {
    axios('/cardsList.json')
      .then(r => dispatch({type: 'SET_PRODUCTS', payload: r.data}))
      .catch(err => console.log(err))
  }, [])

  const favorites = cardList.filter(card => favorList.includes(card.article))

  return (
    <div className={'favorites'}>
      <h1 className={'favorites__title'}>Favorites</h1>
      {favorites.length <= 0 && <p className='no-items'>No items in favorites</p>}
      <CardList itFavor cardInFavor={favorites}/>
    </div>
  );

}

export default Favorites;