export const IS_REQUIRED = 'This field is required'
export const PHONE_REGEX = /^(\+)?(\(\d{2,3}\) ?\d|\d)(([ \-]?\d)|( ?\(\d{2,3}\) ?)){5,12}\d$/
export const STR_REGEX = /[A-Za-z]/
export const NUM_REGEX = /^(\d){2,3}$/g