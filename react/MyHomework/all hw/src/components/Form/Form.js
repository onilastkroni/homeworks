import React from 'react';
import './Form.scss'
import {Formik, Form as form, Field} from 'formik';
import * as yup from 'yup';
import MyInput from "../MyInput/MyInput";
import {STR_REGEX, PHONE_REGEX, NUM_REGEX, IS_REQUIRED} from './Regex'
import {useDispatch} from "react-redux";

const schema = yup.object().shape({
  firstname: yup.string()
    .matches(STR_REGEX, 'Use only character, not number or smt else')
    .required(IS_REQUIRED),
  lastname: yup.string()
    .matches(STR_REGEX, 'Use only character, not number or smt else')
    .required(IS_REQUIRED),
  age: yup.string('This field should be a number')
    .matches(NUM_REGEX, 'Invalid input')
    .required(IS_REQUIRED),
  address: yup.string()
    .min(10, 'Minimum 10 characters for delivery address')
    .required(IS_REQUIRED),
  phone: yup.string()
    .matches(PHONE_REGEX, 'Invalid phone number, try again')
    .required('This field is required'),
})

export const Form = ({cart}) => {
  const dispatch = useDispatch()
  const prodName = cart.map(card => card.name + ';')
  return (
    <Formik initialValues={{
      firstname: '',
      lastname: '',
      age: '',
      address: '',
      phone: ''
    }}
            onSubmit={(values, {setSubmitting}) => {
              localStorage.setItem('cartList', JSON.stringify([]))
              let updCartList = localStorage.getItem('cartList')
              dispatch({type:'MAKE_DELIVERY', payload:updCartList})
              console.log(JSON.stringify(values, null, 2));
              console.log('Album names that you bought:\n',...prodName,)
              setSubmitting(false);
            }}
            validationSchema={schema}
    >
      {(formikProps) => {
        return (
          <form noValidate onSubmit={formikProps.handleSubmit}>
            <h2 className={'form-title'}>Delivery information</h2>
            <div>
              <Field
                data-testid = "firstname"
                component={MyInput}
                name="firstname"
                type="text"
                placeholder="Firstname"
              />
            </div>
            <div>
              <Field
                data-testid = "lastname"
                component={MyInput}
                name="lastname"
                type="text"
                placeholder="Lastname"
              />
            </div>

            <div>
              <Field
                data-testid = "age"
                component={MyInput}
                name="age"
                type="text"
                placeholder="Age"
              />
            </div>

            <div>
              <Field
                data-testid = "address"
                component={MyInput}
                name="address"
                type="text"
                placeholder="Address"
              />
            </div>

            <div>
              <Field
                data-testid = "phone"
                component={MyInput}
                name="phone"
                type="text"
                placeholder="Phone"
              />
            </div>
            <div>
              <button className={'sbmt-button'} type="submit">Checkout</button>
            </div>
          </form>
        )
      }}
    </Formik>
  );
};

export default Form;