import {render} from '@testing-library/react'
import Button from "./Button";
import store, {reducer} from '../../store/store'
import userEvent from "@testing-library/user-event";
import Form from "../Form/Form";
import {Provider} from "react-redux";


const types = {
  SET_TO_FAVORITE: 'SET_TO_FAVORITE',
  DELETE_FROM_FAVORITE: 'DELETE_FROM_FAVORITE'
}

describe('Button testing', () => {
  test('render button', () => {
    render(<Button/>)
  })
  test('SET_TO_FAVORITES sets data to data props ', () => {
    const testFavorites = ['2133', '1233', '8481']
    const action = {
      type: types.SET_TO_FAVORITE,
      payload: testFavorites
    }
    let newState = {}
    const addToCartTest = jest.fn(() => {
      const favorList = {
        data: ['1231', '3133'],
        isLoading: true,
        error: null
      }
      newState = reducer(favorList, action)
      return newState
    })
    const {getByTestId} = render(<Button handleClick={addToCartTest}/>)
    const addButton = getByTestId('btn')
    userEvent.click(addButton)
    expect(addToCartTest).toHaveBeenCalled()
    expect(newState.favorList.data).toStrictEqual(testFavorites)
  })

  test('DELETE_FROM_FAVORITE delete data from data props', () => {
    const testFavorites = ['1233', '8481']
    const action = {
      type: types.DELETE_FROM_FAVORITE,
      payload: testFavorites
    }
    let newState = {}
    const delFromCart = jest.fn(() => {
      const favorList = {
        data: ['1231', '3133'],
        isLoading: true,
        error: null
      }
      newState = reducer(favorList, action)
      return newState
    })
    const {getByTestId} = render(<Button handleClick={delFromCart}/>)
    const delButton = getByTestId('btn')
    userEvent.click(delButton)
    expect(delFromCart).toHaveBeenCalled()
    expect(newState.favorList.data).toStrictEqual(testFavorites)
  })
  test('Testing form for make order', () => {
    const mockBuyFunc = jest.fn()

    const {getByTestId} = render(<Button handleClick={mockBuyFunc}/>)
    const testingCart = ['1233']
    const form = render(
      <Provider store={store}>
        <Form cart={testingCart}/>
      </Provider>
    )

    const firstname = form.getByTestId('firstname')
    const lastname = form.getByTestId('lastname')
    const age = form.getByTestId('age')
    const address = form.getByTestId('address')
    const button = getByTestId('btn')

    userEvent.type(firstname, 'John')
    userEvent.type(lastname, 'Jones')
    userEvent.type(age, '20')
    userEvent.type(address, '12a street')

    expect(mockBuyFunc).not.toHaveBeenCalled()
    userEvent.click(button)

    expect(firstname.value).toBe('John')
    expect(lastname.value).toBe('Jones')
    expect(age.value).toBe('20')
    expect(address.value).toBe('12a street')
    expect(mockBuyFunc).toHaveBeenCalledTimes(1)

  })
})