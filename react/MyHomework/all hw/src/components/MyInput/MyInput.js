import React from 'react';

const MyInput = ({field, form, ...rest}) => {
  const {name} = field;
  return (
    <div className={'input-place'}>
      <input className={'field'} {...rest} {...field} />
      <div>
        {form.touched[name] && form.errors[name] && (
          <div className="error">{form.errors[name]}</div>
        )}
      </div>
    </div>
  );


};

export default MyInput;