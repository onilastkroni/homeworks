import React, {useEffect, useRef} from 'react';
import CardList from "../CardList/CardList";
import './Cart.scss'
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import Form from "../Form/Form";


const Cart = () => {
  const cartList = useSelector(state => state.cartList.data)
  const cardList = useSelector(state => state.cardList.data)
  const dispatch = useDispatch()
  const lorinRef = useRef(null)

  useEffect(() => {
    axios('/cardsList.json')
      .then(r => dispatch({type: 'SET_PRODUCTS', payload: r.data}))
      .catch(err => console.log(err))
  }, [])

  const scrollToForm = ()=>{
    lorinRef.current.scrollIntoView()
  }
  const cart = cardList.filter(card => cartList.includes(card.article))
  const priceCounter = cart.map(card => +card.price)

  let finalPrice = null
  if (priceCounter.length > 0) {
    finalPrice = priceCounter.reduce((a, b) => a + b)
  }

  return (
    <div className={'cart'}>
      <h1 className={'cart__title'}>Cart</h1>
      {cart.length > 0 && <div className={'order-info'}>
        <div className={'price-counter'}>Total price: {finalPrice}$</div>
        <div className={'order-btn'} onClick={scrollToForm}>Make order</div>
      </div>}
      {cart.length <= 0 && <p className='no-items'>Your cart is empty</p>}
      <CardList itCart cardsInCart={cart}/>
      {cart.length > 0 && <Form cart={cart}/>}
      <p ref={lorinRef}></p>
    </div>
  );
}

export default Cart;