const {createStore} = require("redux");

const initialState = {
  favorList: {
    data: JSON.parse(localStorage.getItem('favorList')) || [],
    isLoading: true,
    error: null
  },
  cartList: {
    data: JSON.parse(localStorage.getItem('cartList')) || [],
    isLoading: true,
    error: null
  },
  cardList: {
    data: [],
    isLoading: true,
    error: null
  },
}


export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_TO_CART': {
      return {
        ...state, cartList: {data: [...action.payload], isLoading: false}
      };
    }
    case 'DELETE_FROM_CART': {
      return {
        ...state, cartList: {data: [...action.payload], isLoading: false}
      };
    }
    case 'SET_TO_FAVORITE': {
      return {
        ...state, favorList: {data: [...action.payload], isLoading: false}
      };
    }
    case 'DELETE_FROM_FAVORITE': {
      return {
        ...state, favorList: {data: [...action.payload], isLoading: false}
      };
    }
    case 'SET_PRODUCTS': {
      return {
        ...state, cardList: {data: [...action.payload], isLoading: false}
      };
    }
    case 'MAKE_DELIVERY': {
      return {
        ...state, cartList: {data: [...action.payload], isLoading: false},
      }
    }
    default:
      return state
  }
}

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;