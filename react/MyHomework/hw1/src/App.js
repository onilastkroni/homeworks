import './App.css';
import Button from "./components/Button/Button";
import React from "react";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    state = {
        isFirstModalOpen: false,
        isSecondModalOpen: false
    }

    handleClick = (modal) => {
        if (modal === 'first') {
            this.setState({isFirstModalOpen: true, isSecondModalOpen: false})
        } else this.setState({isSecondModalOpen: true, isFirstModalOpen: false})
    }

    closeModal = () => {
        this.setState({isFirstModalOpen: false, isSecondModalOpen: false})
    }

    render() {
        return (
            <div className="App">
                <Button text='Open first modal' backgroundColor='blue' onClick={(first) => this.handleClick('first')}/>
                <Button text='Open second modal' backgroundColor='green' onClick={(second) => this.handleClick()}/>
                {this.state.isFirstModalOpen &&
                <Modal header='Do you want to delete this file?' closeButton={true} text='First Modal'
                       close={this.closeModal}
                       actions={
                           <>
                               <Button text='Apply'/>
                               <Button text='Close'/>
                           </>}
                />}
                {this.state.isSecondModalOpen &&
                <Modal header='Your Modal' closeButton={true} text='Second Modal' close={this.closeModal}
                       actions={
                           <>
                               <Button text='Apply'/>
                               <Button text='Close'/>
                           </>
                       }
                />}
            </div>
        );
    }

}

export default App;
