import './Modal.scss'
import Button from "../Button/Button";

import React, {Component} from "react";

class Modal extends Component {
    render() {
        const {header, closeButton, text, actions, close} = this.props
        return (
            <div className={'modal'}>
                <div className={'modal__window'} >
                    <header className={'modal__window-header'}>{header} {closeButton &&
                    <span onClick={close} className={'modal__window-button'}>X</span>}</header>
                    <p className={'modal__window-text'}>{text}</p>
                    <div  onClick={close}>{actions}</div>
                </div>
            </div>
        )
    }
}

export default Modal