const btn = document.querySelector('#btn')
const link = document.querySelector('link')
btn.addEventListener('click', changeTheme)

function changeTheme() {
    const theme = localStorage.getItem('theme')
    let style;
    if (theme !== null) {
        style = theme.includes('2') ? './css/style.css' : './css/style2.css'

    } else {
        style = './css/style2.css'
    }
    localStorage.setItem('theme', style)
    link.href = style
}

const themeCheck = localStorage.getItem('theme')
if (themeCheck !== null) link.href = themeCheck