const buttons = document.querySelectorAll('.btn')
buttons.forEach(elem => {
    window.addEventListener('keypress' , (event) => {
        if (event.key.toLowerCase() === elem.innerText.toLowerCase()) {
            elem.style.background = 'blue'
        } else {
            elem.style.background = 'black'
        }
    })
})