const input1 = document.getElementById('input-1')
const input2 = document.getElementById('input-2')

const i1 = document.querySelector('.i-1')
const i2 = document.querySelector('.i-2')

const btn = document.querySelector('.btn')

const text = document.createElement('span')
text.innerText = 'Нужно ввести одинаковые значения'
text.style.color = 'red'


i1.addEventListener('click', (click))


function click() {
    if (input1.type === 'password') {
        i1.classList.replace('fa-eye', 'fa-eye-slash')
        input1.type = 'text'
    } else if (input1.type === 'text') {
        i1.classList.replace('fa-eye-slash', 'fa-eye')
        input1.type = 'password'
    }
}


i2.addEventListener('click', (click2))


function click2() {
    if (input2.type === 'password') {
        i2.classList.replace('fa-eye', 'fa-eye-slash')
        input2.type = 'text'
    } else if (input2.type === 'text') {
        i2.classList.replace('fa-eye-slash', 'fa-eye')
        input2.type = 'password'
    }
}


btn.addEventListener('click', (event) => {
    event.preventDefault()
    if (input1.value === input2.value && input1.value != '' && input2.value != '') {
        alert('You are welcome')
    } else {
        input2.insertAdjacentElement('afterend', text)
    }
})


