document.addEventListener("DOMContentLoaded", function () {
    const stop = document.querySelector("#stop")
    const resume = document.querySelector("#resume")
    const images = document.querySelectorAll('.image-to-show')

    let i = 0
    let timer = setInterval(function () {
        images[i].style.display = "none";
        i++
        if (i === images.length) {
            i = 0
            images[i].style.display = "block"
        } else {
            images[i].style.display = "block"
        }
    }, 3000);

    stop.addEventListener('click', function () {
        clearInterval(timer)
    });

    resume.addEventListener('click' , function () {
        timer = setInterval(function () {
            images[i].style.display = "none"
            i++
            if (i === images.length) {
                i = 0
                images[i].style.display = "block"
            } else {
                images[i].style.display = "block"
            }
        },3000)
    })
})