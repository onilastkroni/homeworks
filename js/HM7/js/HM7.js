function getString(arr){
    let str = "<ul>";
    arr.forEach(elem => {
        if(Array.isArray(elem)){
            str += `<li>${getString(elem)}</li>`;
        }else{
            str += `<li>${elem}</li>`;
        }
    })
    return str + "</ul>";
}

function append(array, elem = document.body){
    const list = document.createElement('div');
    list.innerHTML = getString(array);
    elem.append(list)
}

const place = document.querySelector('#app')

append(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], place);