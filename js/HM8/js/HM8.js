const input = document.querySelector('.input')
const span = document.createElement('span')
const btn = document.createElement('button')
const text = document.createElement('span')
text.textContent = 'Please enter correct price'
text.style.display = 'block'
btn.textContent = 'X'
span.style.display = 'block'

input.addEventListener('focus', (event) => {
    input.style.border = '3px solid green'
    input.style.borderRadius = `5px`
})

input.addEventListener('blur' , (event) => {
    input.style.border = `1px solid grey`
    input.style.borderRadius = `none`
    span.textContent = 'Текущая цена:' + input.value
    span.append(btn)
    input.insertAdjacentElement('beforebegin' , span)
    input.style.color = 'darkgreen'
    text.remove()

    if (input.value < 0) {
input.style.border = '3px solid red'
        span.remove()
        input.insertAdjacentElement('afterend' , text)
    }
})

btn.addEventListener('click' , (event) => {
    span.remove()
    btn.remove()
    input.value = ''
})