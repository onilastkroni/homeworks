let number = +prompt("Enter your number")
while (!Number.isInteger(number)) {
    number = +prompt("Enter your number again")
}
if (number<5 || number<0 ) {
    console.log("Sorry no numbers")
} else {
    for (let i = 1; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
}